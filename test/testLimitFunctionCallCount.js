const limitFunctionCallCount=require('../limitFunctionCallCount');
//test1

function cb(){
    let name="ayushi";
    console.log(name);
}
const result=limitFunctionCallCount(cb,2);
result();
result();
result();
result();
result();


//test2
/*
const add= (a,b)=>  a+b ;                                                                                                 ;
const limitedAddFn=limitFunctionCallCount(add,2);

console.log(limitedAddFn(1,2));
console.log(limitedAddFn(3,4));
console.log(limitedAddFn(4,5));
*/

//test3
/*
const doMath= (a,b,c)=>  a+b-c ;                                                                                                ;
const limitedMathFn=limitFunctionCallCount(doMath,3);

console.log(limitedMathFn(1,2,3));
console.log(limitedMathFn(3,4,5));
console.log(limitedMathFn(4,5,1));
console.log(limitedMathFn(4,5,6));
*/

