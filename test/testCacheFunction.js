
const cacheFunction=require('../cacheFunction')
//test1
/*
function tb(item){
    let name="ayushi";
    console.log(name,item);
    return (item+" Mathur");
}

const result=cacheFunction(tb);
console.log(result("twinkle"));
console.log(result("priya"));
console.log(result("supriya"));
console.log(result("rohini"));
*/

//test2

const add= (a,b)=>  a+b ;                                                                                                 ;
const cacheAdd=cacheFunction(add);
console.log(cacheAdd(1,2));
console.log(cacheAdd(2,3));
console.log(cacheAdd(1,2));
console.log(cacheAdd(2,3));
//console.log(cacheAdd(2,3));

//test3
/*
const cachedDoMath= (a,b,c)=>  a+b-c ;                                                                                                ;
const cachedDoMAth=cacheFunction(cachedDoMath);

console.log(cachedDoMAth(1,2,3));
console.log(cachedDoMAth(1,2,3));
console.log(cachedDoMAth(1,3,2));
console.log(cachedDoMAth(2,3,4));
console.log(cachedDoMAth(2,3,4));
*/
//test 4
/*
const square= (num)=>  num*num;                                                                                                ;
const cachedSquare=cacheFunction(square);

console.log(cachedSquare(2));
console.log(cachedSquare(2));
console.log(cachedSquare(3));
console.log(cachedSquare(3));
console.log(cachedSquare(4));
*/
