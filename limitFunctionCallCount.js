function limitFunctionCallCount(cb, n) {
    let count=0
    function functioncall(arg1,arg2,arg3,arg4){
        if(count<n){
            count++;
            return cb(arg1,arg2,arg3,arg4);
        
        }
        else{
            return null;
        }
    }
    return functioncall;
}
module.exports=limitFunctionCallCount;