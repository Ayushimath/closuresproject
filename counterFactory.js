//counterfactory

function counterFactory() {
    let counter=0;

    let counterobj={
         increment:()=>{
            counter++;
            return counter;
        },

        decrement:()=>{
            counter--;
            return counter;
        }
    };
    return counterobj;
}

module.exports=counterFactory;