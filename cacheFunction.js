//cacheFunction
/*
function cacheFunction(cb) {

    let cache={};
    let array=[];
    function functioncall(arg1,arg2,arg3){
        //console.log(arg1,arg2,arg3);
        //adding argument
        if(Object.keys(cache).length === 0){
            
            cache["argument1"]=array.concat([arg1]);

            if(arg2!==undefined){
            cache["argument2"]=array.concat([arg2]);
            }
            if(arg3!==undefined){
            cache["argument3"]=array.concat([arg3]);
            }
        }
        else{
            //check argument already exist or not
            let find1=false;
            for(let i=0;i<cache["argument1"].length;i++){
                if(cache["argument1"][i]===arg1){ 
                    find1=true;
                }
            }

            //for argument2
            let find2;
            if(arg2!==undefined){
                find2=false;
                for(let i=0;i<cache["argument2"].length;i++){
                    if(cache["argument2"][i]===arg2){ 
                        find2=true;
                    }
                }
            }
            
            //for argument3
            let find3;
            if(arg3!==undefined){
                find3=false;
                for(let i=0;i<cache["argument3"].length;i++){
                    if(cache["argument3"][i]===arg3){ 
                        find3=true;
                    }
                }
            }
           
            
            //console.log(find1);
            //console.log(find2);
            //console.log(find3);
            
            //if we didnt find the argument in cache
            //we will add that in cache
            //if we find we will return cache 
            if((find1===true) && (find2===true || find2===undefined)&&(find3===true || find3===undefined)){
                return cache;
            }
            else{
                cache["argument1"]=cache["argument1"].concat([arg1]);
                
                if(find2!==undefined){
                    cache["argument2"]=cache["argument2"].concat([arg2]);
                }

                if(find3!==undefined){
                    cache["argument3"]=cache["argument3"].concat([arg3]);
                }    

            }
        }
     
        //console.log(cache);
        //invoking cb
        return cb(arg1,arg2,arg3);
    }
    
    return functioncall;
}

module.exports=cacheFunction;
*/
//cacheFunction
function cacheFunction(cb) {

    let cache={};
    
    function functioncall(arg1,arg2,arg3){
        //console.log(arg1,arg2,arg3);
        //adding argument
        if(Object.keys(cache).length === 0){
            if(arg1!==undefined && arg2!==undefined && arg3!==undefined){
                cache["["+arg1+","+arg2+","+arg3+"]"]=cb(arg1,arg2,arg3);
                //console.log("first 3key",cache);

                return cache["["+arg1+","+arg2+","+arg3+"]"];

            }
            else if(arg1!==undefined && arg2!==undefined && arg3===undefined){
                cache["["+arg1+","+arg2+"]"]=cb(arg1,arg2);
                //console.log("first 2key",cache);
                return cache["["+arg1+","+arg2+"]"];
            }
            else if 
            (arg1!==undefined && arg2===undefined && arg3===undefined){
                cache["["+arg1+"]"]=cb(arg1);
                //console.log("first 1key",cache);
                return cache["["+arg1+"]"];
            }
        }
        else{
            //check argument already exist or not
            let find=false;
            for(let property in cache){
                if(arg1!==undefined && arg2!==undefined && arg3!==undefined){
                    if(property==="["+arg1+","+arg2+","+arg3+"]"){
                        find=true;
                        //console.log("find3",cache);
                        return(cache[property]);
                    }
                }    
                
                else if(arg1!==undefined && arg2!==undefined && arg3===undefined){
                    
                    //console.log(property);
                    if(property==="["+arg1+","+arg2+"]"){
                        find=true;
                        //console.log("find2",cache);
                        return(cache[property]);
                    }
                }
                else if(arg1!==undefined && arg2===undefined && arg3===undefined){
                    if(property==="["+arg1+"]"){
                        find=true;
                        //console.log("find1",cache);
                        return(cache[property]);
                    }
                }
            }
                //we didnt find the key in cache
                //we will add and return the result
                if (find===false){
                    if(arg1!==undefined && arg2!==undefined && arg3!==undefined){
                        cache["["+arg1+","+arg2+","+arg3+"]"]=cb(arg1,arg2,arg3);
                        //console.log("new obj3",cache);
                        return cache["["+arg1+","+arg2+","+arg3+"]"];
                    }
                    else if(arg1!==undefined && arg2!==undefined && arg3===undefined){
                        cache["["+arg1+","+arg2+"]"]=cb(arg1,arg2);
                        //console.log("new obj2",cache);
                        return cache["["+arg1+","+arg2+"]"];
                    }
                    else if 
                    (arg1!==undefined && arg2===undefined && arg3===undefined){
                        cache["["+arg1+"]"]=cb(arg1);
                        //console.log("new obj1",cache);
                        return cache["["+arg1+"]"];
                    }

                }
            
        }        
    }
    return functioncall;
}

module.exports=cacheFunction;